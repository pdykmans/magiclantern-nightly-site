'use strict';

function CamerasCtrl($scope, JenkinsService, ChangelogService, BuildOutputService) {

	function init() {
		$scope.loading = false;
		$scope.builds = [];
		$scope.consoleText = [];
		$scope.platforms = JenkinsService.platforms();
	}

	$scope.isError = function(build) {
		if(!build || build.result !== 'SUCCESS') {
			return true;
		} else {
			return false;
		}
	};
	
	$scope.showChangelog = function(build) {
		ChangelogService.show(build);
	};

	$scope.downloadURL = function(build) {
		if(build) {
			return JenkinsService.downloadURL($scope.platform, build);
		}
	};

	$scope.buildOutput = function(build) {
		BuildOutputService.show(JenkinsService.consoleText($scope.platform, build));
	};

	init();

	$scope.$watch('platform', function(platform) {
		if(platform && platform.name) {
			$scope.loading = true;
			$scope.builds = JenkinsService.builds(platform.name);
		}
	});

	$scope.$watch('builds', function(builds) {
		if(builds) {
			$scope.loading = false;
			$scope.latestBuild = builds[0];
		}
	});
}

angular.module('nightlyApp').controller('CamerasCtrl', CamerasCtrl);
