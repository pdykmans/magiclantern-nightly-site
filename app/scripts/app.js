'use strict';

var app = angular.module('nightlyApp', ['ui', 'ui.router', 'ui.bootstrap', 'ui.route', 'restangular']);

app.config(function ($stateProvider, $urlRouterProvider, RestangularProvider) {
	RestangularProvider.setBaseUrl('http://builds.magiclantern.fm/jenkins/');
	RestangularProvider.setRequestSuffix('/api/json');
	$stateProvider
	.state('home', {
		url: '/cameras',
		templateUrl: 'views/cameras.html',
		controller: 'CamerasCtrl'
	})
	.state('modules', {
		url: '/modules',
		templateUrl: 'views/modules.html',
		controller: 'ModulesCtrl'
	});
	$urlRouterProvider.otherwise('/cameras');
});
